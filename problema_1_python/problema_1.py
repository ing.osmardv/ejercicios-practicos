import sys
import os.path
from os import path


def getFile(): 
    if(path.exists(sys.argv[1])):
        file = sys.argv[1]
    else:
        raise Exception("El archivo no existe")

    f = open ('entrada.txt','r')
    file_text = f.read()
    f.close()
    return file_text

def getLines():
    text =  getFile().split('\n')

    for index, line in enumerate(text):
        if( index == 0 ):
            lengths = line.split(' ')
            if( len(lengths) != 3 ):
                raise Exception ('La linea 1 debe tener 3 valores que corresponden a las longitudes de la instruccion 1, instruccion 2 y el mensaje separados unicamente por un espacio')
            for length in lengths:
                if( not length.isdecimal()):
                    raise Exception ('La linea 1 debe tener 3 valores enteros')
        if( index == 1 ):
            first_instruction = line
        if( index == 2 ):
            second_instruction = line
        if( index == 3 ):
            message = line
    
    return [lengths, first_instruction, second_instruction, message]

def getOutput():
    clean_message = ''
    [lengths, first_instruction, second_instruction, message] = getLines()

    
    if(valideLengths(len(first_instruction), 2, 50, lengths[0]) != 'False'):
        msg = valideLengths(len(first_instruction), 2, 50, lengths[0] )
        raise Exception('La longitud de la primera instruccion ' + msg)
    if(valideLengths(len(second_instruction), 2, 50, lengths[1]) != 'False'):
        msg = valideLengths(len(second_instruction), 2, 50, lengths[1])
        raise Exception('La longitud de la segunda instruccion ' + msg)
    if(valideLengths(len(message), 2, 5000, lengths[2]) != 'False'):
        msg = valideLengths(len(message), 3, 5000, lengths[2])
        raise Exception('La longitud del mensaje ' + msg)

    if( repeatLetterInstruction(first_instruction)):
        raise Exception('La instruccion 1 tiene caracteres continuos repetidos')
    if( repeatLetterInstruction(second_instruction)):
        raise Exception('La instruccion 2 tiene caracteres continuos repetidos')

    if(not message.isalnum()):
        raise Exception ('El mensaje debe estar conformado unicamente por caracteres alfanumericos')

    for letter in message:
        if( len(clean_message) == 0 ):
            clean_message += letter
        elif( clean_message[-1] != letter):
            clean_message += letter
            
    if (first_instruction in clean_message):
        success_1 = 'SI'
    else:
        success_1 = 'NO'

    if (second_instruction in clean_message):
        success_2 = 'SI'
    else:
        success_2 = 'NO'

    if (success_1 == 'SI' and success_2 == 'SI'):
        raise Exception('Se han encontrado las 2 instrucciones dentro del mensaje y solo debe de existir una')

    return success_1 + "\n" + success_2

def printOutput():
    output = getOutput()
    f = open("salida.txt", "w")
    f.write(output)
    f.close()
    print('Listo!')


def valideLengths(length, min, max, expected):
    if (int(length) != int(expected) ):
        return 'no coincide con la especificada en la linea 1. \nEsperada: %s \nRecibida: %s' % ( expected, length)
    if (int(length) > int(max) or int(length) < int(min)) :
        return 'no esta dentro del rango. \nRecibida: %s \nMinimo: %s\nMaximo:%s' % (expected, min, max)
    return 'False'

def repeatLetterInstruction(instruction):
    last = ''
    for letter in instruction :
        if (last == letter) :
            return True
        last = letter
    return False


try:
    printOutput()
except Exception as error:
    print(error)
