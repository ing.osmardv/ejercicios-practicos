const readline = require("readline");
const fs = require("fs");
const file = (process.argv[2]) ? process.argv[2] : "entrada.txt";

if (!fs.existsSync(file)) {
    console.log('El archivo no existe!');
    return;
}

let text = readline.createInterface({
    input: fs.createReadStream(file)
});

const getLines = () => {
    let first = true;
    let scores = [];
    let rounds = 0;
    return new Promise((resolve, reject) => {
        text.on("line", linea => {
            if (first) {
                first = !first;
                rounds = parseInt(linea);
            } else {
                scores = [...scores, linea.split(' ').map(Number)];
            }
        });

        text.on('error', reject)

        text.on('close', function () {
            resolve({rounds, scores})
        });

    });
}

const winner = async () => {
    let result = [0, 0];
    let leader = [0, 0];
    const {rounds, scores} = await getLines();
    if( rounds > 10000 || isNaN(rounds)){
        throw new Error(`La primera linea debe de ser un numero entero menoro igual a 10000`)
    }

    if( scores.length !== rounds){
        throw new Error(`La cantidad de rondas dentro el archivo es diferente a la especificada en la primera linea`);
    }

    scores.map((round_score, index) => {
        if(round_score.length !== 2 ){
            throw new Error(`La cantidad de marcadores es erroneo dentro de la ronda ${index  + 1 }`);
        }
        if(isNaN(round_score[0]) || isNaN(round_score[1])){
            throw new Error(`Existe un valor erroneo dentro la ronda ${index  + 1 }`);
        }
    })

    scores.map(round_score => {
        result = (round_score[0] > round_score[1]) ? [1, round_score[0] - round_score[1]] : [2, round_score[1] - round_score[0]];
        if (result[1] > leader[1]) {
            leader = [...result];
        }
    });

    fs.writeFile('salida.txt', `${leader[0]} ${leader[1]}`, function (err) {
        if (err) throw err;
        console.log('Listo!');
    });
}

winner().catch(err => console.error(err))