const readline = require("readline");
const fs = require("fs");

const file = (process.argv[2]) ? process.argv[2] : "entrada.txt";

if( !fs.existsSync(file) ){
    throw new Error('El archivo no existe!')
}

const text = readline.createInterface({
    input: fs.createReadStream(file)
})


const getLines = () => {
    let cont = 0;
    let lengths;
    let first_instruction;
    let second_instruction;
    let message;
    return new Promise(resolve => {
        text.on("line", linea => {
            if (cont === 0) lengths = linea.split(' ').map(Number);
            if (cont === 1) first_instruction = linea;
            if (cont === 2) second_instruction = linea;
            if (cont === 3) {
                message = linea;
                resolve({ lengths, first_instruction, second_instruction, message })
            };
            cont++;
        });
    });
}

const printResponse = async () => {
    const { lengths, first_instruction, second_instruction, message } = await getLines();
    let clean_message = '';

    if( lengths.length !== 3 ){
        throw new Error( `La linea 1 debe tener 3 valores que corresponden a las longitudes de la instruccion 1, instruccion 2 y el mensaje separados unicamente por un espacio`);
    }
    if(lengths.includes(NaN)){
        throw new Error( `La linea uno debe estar unicamente conformada por numeros enteros que representas las longitudes  de la instruccion 1, instruccion 2 y el mensaje respectivamente`);
    }

    if (valideLengths(first_instruction.length, 2, 50, lengths[0])) {
        throw new Error(`La longitud de la primera instruccion ${valideLengths(first_instruction.length, 2, 50, lengths[0])}`)
    }
    if (valideLengths(second_instruction.length, 2, 50, lengths[1])) {
        throw new Error(`La longitud de la segunda instruccion ${valideLengths(second_instruction.length, 2, 50, lengths[1])}`)
    }
    if (valideLengths(message.length, 3, 5000, lengths[2])) {
        throw new Error(`La longitud del mensaje ${valideLengths(message.length, 3, 5000, lengths[2])}`)
    }

    if(repeatLetterInstruction(first_instruction)){
        throw new Error(`La instruccion 1 tiene caracteres continuos repetidos`)
    }

    if(repeatLetterInstruction(second_instruction)){
        throw new Error(`La instruccion 2 tiene caracteres continuos repetidos`)
    }

    for (let x = 0; x < lengths[2]; x++) {
        if (!/[a-zA-Z0-9]/.test(message[x])) {
            throw new Error(`El mensaje debe estar conformado unicamente por caracteres alfanumericos`);
        }
        if (clean_message[clean_message?.length - 1] !== message[x]) {
            clean_message += message[x];
        }
    }

    let success_1 = (clean_message.includes(first_instruction)) ? 'SI' : 'NO';
    let success_2 = (clean_message.includes(second_instruction)) ? 'SI' : 'NO';
    if( success_1 === 'SI' && success_2 === 'SI'){
        throw new Error(`Se han encontrado las 2 instrucciones dentro del mensaje y solo debe de existir una`)
    }
    createOutput(`${success_1}\n${success_2}`);

}

valideLengths = (length, min, max, expected) => {
    if(expected % 1 !== 0){
        return `que se recibe en la primera linea debe de ser un valor entero`;
    }
    if (length !== expected) {
        return `no coincide con la especificada en la linea 1. \nEsperada: ${expected} \nRecibida: ${length}`
    }
    if (length > max || length < min ) {
        return `no esta dentro del rango. \nRecibida: ${length} \nMinimo: ${min}\nMaximo:${max}`
    }
    return false;
}

repeatLetterInstruction = (instruction) => {
    let last = ''
    for (let x = 0; x < instruction.length; x++) {
        if (last === instruction[x]) {
            return true
        }
        last = instruction[x]
    }
    return false
}

createOutput = (message) => {
    fs.writeFile('salida.txt', message, function (err) {
        if (err) throw err;
        console.log('Listo!');
    });
}

printResponse().catch(err => console.error(err))

